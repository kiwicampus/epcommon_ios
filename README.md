# EPCommon

[![CI Status](http://img.shields.io/travis/Yeisson Oviedo/EPCommon.svg?style=flat)](https://travis-ci.org/Yeisson Oviedo/EPCommon)
[![Version](https://img.shields.io/cocoapods/v/EPCommon.svg?style=flat)](http://cocoapods.org/pods/EPCommon)
[![License](https://img.shields.io/cocoapods/l/EPCommon.svg?style=flat)](http://cocoapods.org/pods/EPCommon)
[![Platform](https://img.shields.io/cocoapods/p/EPCommon.svg?style=flat)](http://cocoapods.org/pods/EPCommon)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EPCommon is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "EPCommon", :git => 'git@bitbucket.org:kiwicampus/epcommon_ios.git'
```

## Author

Jason Oviedo, jason@kiwicampus.coms

## License

EPCommon is available under the MIT license. See the LICENSE file for more info.
