#
# Be sure to run `pod lib lint EPCommon.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "EPCommon"
  s.version          = "1.0"
  s.summary          = "Kiwi common libs/utilities for standard Apps"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
Equivalent of EPCommon Android lib. There are quite a bit of problems that are already
solved. These library also standardize the way Kiwi apps should be implemented
                       DESC

  s.homepage         = "https://bitbucket.org/kiwicampus/epcommon_ios"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Jason Oviedo" => "jason@kiwicampus.com" }
  s.source           = { :git => "git@bitbucket.org:sanduche/epcommon_ios.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '10.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'EPCommon' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'MBProgressHUD', '~> 1.1.0'
  s.dependency 'Toast', '~> 4.0.0'
  s.dependency 'ReachabilitySwift', '~> 4'
  s.dependency 'EasyAnimation'
end
