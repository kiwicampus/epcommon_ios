//
//  Util.swift
//  Kiwi
//
//  Created by Yeisson Oviedo on 12/13/15.
//  Copyright © 2015 Onlulo, Inc. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toast
import EasyAnimation
//import Crashlytics
import Reachability

open class Util{
    
    public static weak var rootViewController: UIViewController?
    
    public static func test(){
        print("it works")
    }
    
    public static func dismissKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    open class MoveOnKeyboardShowHelper: NSObject{
        var offset: CGFloat = -10
        
        open weak var view: UIView?
        open weak var superView: UIView?
        var origin = CGPoint(x: 0, y: 0)
        
        public convenience init(view:UIView?, superView:UIView){
            self.init(view:view, superView:superView, onKeyboardShow:nil,onKeyboardHide:nil)
        }
        
        public init(view: UIView?, superView:UIView, onKeyboardShow: (() -> Void)?, onKeyboardHide: (() -> Void)?){
            self.view = view
            self.superView = superView
            self.origin = superView.frame.origin
            super.init()
            
            register();
        }
        
        deinit{
            //        AppDelegate.debugPrint("KBHelper is being destroyed")
            deRegister()
        }
        
        open func register(){
            NotificationCenter.default.addObserver(self, selector: #selector(MoveOnKeyboardShowHelper.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(MoveOnKeyboardShowHelper.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
        
        open func deRegister(){
            NotificationCenter.default.removeObserver(self)
        }
        
        @objc func keyboardWillShow(_ notification:Notification){
            let ss = UIScreen.main.bounds
            
            let info: NSDictionary = notification.userInfo! as NSDictionary
            let kbSize = (info.object(forKey: UIResponder.keyboardFrameBeginUserInfoKey)! as AnyObject).cgRectValue.size
            
            guard let b = view?.bounds else {return}
            guard let tf = view?.convert(b, to: nil) else {return}
            
            //Absolute topmost position of keyboard
            let kTop = ss.height - kbSize.height
            
            //Absolute bottom most position of the field we want to move
            let tBottom = tf.maxY //tf.minY + tf.height //- contentViewOffset
            
            var movement = tBottom + offset > kTop ? kTop - tBottom + offset : 0
            movement = min(movement, kbSize.height)
            move(movement)
        }
        
        @objc func keyboardWillHide(_ notification: Notification){
            restore()
        }
        
        open func move(_ movement: CGFloat){
            guard let sf = superView?.frame else {return}
            let bkRect =  CGRect(x: sf.minX, y: sf.minY + movement, width: sf.width, height: sf.height)
            superView?.frame = bkRect
        }
        
        open func restore(){
            guard let sf = superView?.frame else {return}
            let bkRect =  CGRect(x: origin.x, y: origin.y, width: sf.width, height: sf.height)
            superView?.frame = bkRect
        }
    }
    
    
    open class Debug{
        
        public static var enabled = false
        
        public static func print(_ items:Any...){
            if enabled{
                Swift.print(items)
            }
        }
        
        public static func exec(_ block: ()->()){
            if enabled{
                block()
            }
        }
        
    }
    
    open class Run{
        
        static var timers = [String:Int]()
        
        public static func attemptRunAfterLastCallWithDelay(_ delay: TimeInterval, withQueue name:String, block: @escaping ()->()) {
            let tId = (timers[name] ?? 0) + 1
            timers[name] = tId
            let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time){
                let t2Id = timers[name] ?? 0
                if tId == t2Id{
                    block();
                }
            }
        }
        
        
        public static func afterDelay(_ delay: TimeInterval, block: @escaping ()->()) {
            let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time, execute: block)
        }
        
        public static func async(_ block: @escaping ()->()){
            DispatchQueue.global(qos: DispatchQoS.background.qosClass).async{
                block()
            }
        }
        
        public static func inUIThread(_ block: @escaping ()->()){
            if Thread.isMainThread{
                block()
            }else{
                DispatchQueue.main.async {
                    block()
                }
            }
        }
    }
    
    open class Animation{
        public static var defaultErrorAnimation = ErrorAnimations.spring
        
        public enum ErrorAnimations{
            case springAndRedBorder
            case spring
        }
        
        public static func onError(_ view: UIView?, animation: ErrorAnimations? = nil){
            guard let v = view else{return}
            v.layer.removeAllAnimations()
            let anim = animation ?? defaultErrorAnimation
            let color = v.layer.borderColor
            let border = v.layer.borderWidth
            let cornerRadius = v.layer.cornerRadius
            let oFrame = v.frame
            let displacement: CGFloat = 45
            let time: TimeInterval = 0.17
            let springInitialVelocity = 0.01 / CGFloat(time)
            
            UIView.animateAndChain(withDuration:time, delay: 0, options: .curveEaseOut, animations: {
                
                v.frame = CGRect(x:oFrame.minX + displacement, y:oFrame.minY, width:oFrame.width, height: oFrame.height)
                if anim == .springAndRedBorder{
                    v.layer.borderColor = UIColor.red.cgColor
                    v.layer.borderWidth = 2
                    v.layer.cornerRadius = 5
                }
            }, completion: nil).animate(withDuration:time, delay: 0, options: .curveEaseOut, animations: {
                v.frame = CGRect(x:oFrame.minX - displacement,y: oFrame.minY,width: oFrame.width,height: oFrame.height)
                    if anim == .springAndRedBorder{
                        v.layer.borderWidth = 4
                    }
            }, completion: nil).animate(withDuration:time*2, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: springInitialVelocity, options: .curveEaseOut, animations: {
                        v.frame = oFrame
                        if anim == .springAndRedBorder{
                            v.layer.borderColor = color
                            v.layer.borderWidth = border
                            v.layer.cornerRadius = cornerRadius
                        }
                        }, completion: nil)
        }
    }
    
    open class AppInfo{
        public static func version()->String{
            return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "0.0"
        }
        
        public static func build()->String{
            return (Bundle.main.infoDictionary?["CFBundleVersion"] as? String) ?? "0"
        }
    }
    
    open class Alert{
        
        public static func showErrorWithMessage(_ message:String, fromController controller: UIViewController? = nil, andThen handler: ((UIAlertAction?)->Void)? = nil){
            _ = showDismisableWithTitle("Error", withMessage: message, fromController: controller, andThen: handler)
            
        }
        
        public static func showDismisableWithTitle(_ title: String,withMessage message: String, fromController controller: UIViewController? = nil, andThen handler: ((UIAlertAction?)->Void)? = nil)  -> UIAlertController? {
            guard let ctrl = controller ?? Util.rootViewController else {return nil}
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            Util.Run.inUIThread{
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: handler))
                ctrl.present(alertController, animated: true, completion: nil)
                alertController.view.tintColor = Util.Color.navigationTintColor
            }
            return alertController
        }
        
        public static func showNonDismisableWithTitle(_ title:String, withMessage message:String, fromController controller:UIViewController? = Util.rootViewController) -> UIAlertController?{
            guard let ctrl = controller else {return nil}
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            Util.Run.inUIThread{
                
                if let c = Util.Color.navigationTintColor{
                    alertController.view.tintColor = c
                }
                
                ctrl.present(alertController, animated: true, completion: nil)
            }
            return alertController
        }
        
        public static func promptConfirmationWithTitle (_ title: String, message: String, fromController controller: UIViewController? = Util.rootViewController, handler:@escaping ()->Void){
            guard let ctrl = controller else {return}
            let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                handler()
            }))
            
            refreshAlert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "") , style: .default, handler: { (action: UIAlertAction!) in
            }))
            refreshAlert.view.tintColor = Util.Color.navigationTintColor
            ctrl.present(refreshAlert, animated: true, completion: nil)
        }
        
        public static func promptForTextWithTitle(_ title:String, message: String, placeHolder:String, acceptButtonText:String, fromController controller: UIViewController? = Util.rootViewController, block:@escaping (_ text:String)->Void){
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            
            alert.addAction(UIAlertAction(title: acceptButtonText, style: UIAlertAction.Style.default){ _ in
                block(alert.textFields?[0].text ?? "")
                })
            alert.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = placeHolder
                textField.isSecureTextEntry = false
                textField.superview?.layer.borderWidth = 1
            })
            controller?.present(alert, animated: true, completion: nil)
        }
        
        static var hud: MBProgressHUD?
        public static func showBusy(_ anchor: UIViewController? = Util.rootViewController) -> MBProgressHUD?{
            return showBusy(anchor?.view)
        }
        
        public static func showBusy(_ anchor: UIView?) -> MBProgressHUD?{
            hideBusy()
            guard let a = anchor else {return nil}
            Util.Run.inUIThread{
                hud = MBProgressHUD.showAdded(to: a, animated: true)
            }
            Util.Run.attemptRunAfterLastCallWithDelay(15, withQueue: "alert.hideBusy"){
                hideBusy()
            }
            return hud
        }
        
        public static func hideBusy(){
            guard let h = hud else {return}
            Util.Run.inUIThread{
                h.hide(animated: true)
                hud = nil
            }
        }
        
        public static func showToast(_ message:String){
            Util.rootViewController?.view?.makeToast(message)
        }
        
        public static func showLoadingInStatusBar(){
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        public static func hideLoadingInStatusBar(){
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
    }
    
    open class Color{
        public static var navigationTintColor: UIColor?
    }
    
    open class Views{
        public static func createRefreshControl(_ table: UITableView?, target:AnyObject, action:Selector)-> UIRefreshControl{
            let control = UIRefreshControl()
            control.attributedTitle = NSAttributedString(string: "Pull to refresh")
            control.addTarget(target, action: action, for: UIControl.Event.valueChanged)
            table?.addSubview(control)
            return control
        }
        
        public static func createRefreshControl(_ table: UICollectionView?, target:AnyObject, action:Selector)-> UIRefreshControl{
            let control = UIRefreshControl()
            control.attributedTitle = NSAttributedString(string: "Pull to refresh")
            control.addTarget(target, action: action, for: UIControl.Event.valueChanged)
            table?.addSubview(control)
            return control
        }
    }
    
    
    static let reachability: Reachability? = {
//        do {
//            let r = try Reachability.reachabilityForInternetConnection()
//            return r
//        } catch {
//            print("Unable to create Reachability")
//            return nil
//        }
        return Reachability()
    }()
    
    open class Network {
        public static func getDataFromUrlString(_ url:String?, completion: @escaping ((_ data: Data) -> Void), errorBlock: ((_ error:NSError?)->Void)? = nil) {
            guard let u = url else {
                if errorBlock != nil{
                    errorBlock!(nil)
                }
                return
            }
            guard let nsUrl = URL(string: u) else {
                if errorBlock != nil{
                    errorBlock!(nil)
                }
                return
            }
            URLSession.shared.dataTask(with: nsUrl, completionHandler: { (data, response, error) in
                if let e = error{
                    if errorBlock != nil{
                        errorBlock!(e as NSError?)
                    }else{
                        Util.Alert.showErrorWithMessage("Data could not be downloaded")
                    }
                    print(e)
                    return
                }
                guard let d = data else {
                    if errorBlock != nil{
                        errorBlock!(nil)
                    }else{
                        Util.Alert.showErrorWithMessage("No data was received")
                    }
                    return
                }
                completion(d)
                }) .resume()
        }
        
        public static func isWifi() -> Bool{
            return reachability?.connection == .wifi
        }
        
    }
    
}

//func UtilLogCustomEvent(event:String, withCustomAttributes attributes: [String:AnyObject] = [:]){
//
//    let email = UserData.email()
//    let number = UserData.number()
//    let name = UserData.name()
//    var atts = ["name":name, "email":email, "number":number]
//
//    for (k,v) in attributes {
//        atts[k] = String(v)
//    }
//
//    Answers.logCustomEventWithName(event,
//        customAttributes: atts)
//}

//func UtilLogError(message:String, error:ErrorType, withCustomAttributes attributes: [String:AnyObject] = [:]){
//
//
//    let email = UserData.email()
//    let number = UserData.number()
//    let name = UserData.name()
//    var atts: [String:AnyObject] = ["name":name, "email":email, "number":number]
//
//    for (k,v) in attributes{
//        atts[k] = v;
//    }
//
//    for (k,v) in atts{
//        CLSNSLogv("%@: %@",getVaList([k, String(v)]))
//    }
//    CLSNSLogv("Message: %@",getVaList([message]))
//    let e = error as NSError
//
//    Crashlytics.sharedInstance().recordError(e)
//}

//func UtilLogError(event:String, withCustomAttributes attributes: [String:AnyObject] = [:]){
//    let e = NSError(domain: event, code: 0, userInfo: nil)
//    UtilLogError(event, error: e, withCustomAttributes: attributes)
//}



