//
//  CachedImageView.swift
//  Pods
//
//  Created by Yeisson Oviedo on 4/25/16.
//
//

import Foundation

//
//  CachedImageView.swift
//  Kiido
//
//  Created by Yeisson Oviedo on 5/16/15.
//  Copyright (c) 2015 Sanduche. All rights reserved.

import Foundation
import UIKit

protocol AFImageCacheProtocol:class{
    func cachedImageForRequest(_ request:URLRequest) -> UIImage?
    func cacheImage(_ image:UIImage, forRequest request:URLRequest);
}

class AFCacheSingleton{
    static let instance: AFCacheSingleton = {
        AFCacheSingleton()
    }()
    
    init(){
        _defaultImageCache = AFImageCache()
        
        _queue = OperationQueue()
        _queue!.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount

        
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: OperationQueue.main) { (NSNotification) -> Void in
            self.defaultImageCache!.removeAllObjects()
        }
    }
    
    private var _defaultImageCache :AFImageCache?
    var defaultImageCache:AFImageCache? {
        get{
            return _defaultImageCache
        }
    }
    
    private var _queue:OperationQueue?
    var queue:OperationQueue?{
        get{
            return _queue
        }
    }

}


extension UIImageView {
    fileprivate struct AssociatedKeys {
        static var SharedImageCache = "SharedImageCache"
        static var RequestImageOperation = "RequestImageOperation"
        static var URLRequestImage = "UrlRequestImage"
    }
    
    class func setSharedImageCache(_ cache:AFImageCacheProtocol?) {
        objc_setAssociatedObject(self, &AssociatedKeys.SharedImageCache, cache, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
    }
    
    class func sharedImageCache() -> AFImageCacheProtocol {
//        struct Static {
//            static var token : Int = 0
//            static var defaultImageCache:AFImageCache?
//        }
        
//        dispatch_once(&Static.token, { () -> Void in
//            
//            
//        })
        return objc_getAssociatedObject(self, &AssociatedKeys.SharedImageCache) as? AFImageCache ?? AFCacheSingleton.instance.defaultImageCache!
    }
    
    class func af_sharedImageRequestOperationQueue() -> OperationQueue {
//        struct Static {
//            static var token:Int = 0
//            static var queue:OperationQueue?
//        }
        
//        dispatch_once(&Static.token, { () -> Void in
//            
//        })
        return AFCacheSingleton.instance.queue!
    }
    
    fileprivate var af_requestImageOperation:(operation:Operation?, request: URLRequest?) {
        get {
            let operation:Operation? = objc_getAssociatedObject(self, &AssociatedKeys.RequestImageOperation) as? Operation
            let request:URLRequest? = objc_getAssociatedObject(self, &AssociatedKeys.URLRequestImage) as? URLRequest
            return (operation, request)
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.RequestImageOperation, newValue.operation, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            objc_setAssociatedObject(self, &AssociatedKeys.URLRequestImage, newValue.request, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func setImageWithUrlString(_ url:String?, placeHolderImage:UIImage? = nil){
        if (url == nil){
            self.image = placeHolderImage
            return
        }
        if let nsurl = URL(string:url!){
            setImageWithUrl(nsurl)
        }
        else{
            self.image = placeHolderImage
        }
    }
    
    public func setImageWithUrl(_ url:URL, placeHolderImage:UIImage? = nil) {
        let request:NSMutableURLRequest = NSMutableURLRequest(url: url)
        request.addValue("image/*", forHTTPHeaderField: "Accept")
        self.setImageWithUrlRequest(request as URLRequest, placeHolderImage: placeHolderImage, success: nil, failure: nil)
    }
    
    typealias SuccessHandler = (_ request:URLRequest?, _ response:URLResponse?, _ image:UIImage) -> Void
    typealias FailureHandler = ( _ request:URLRequest?,  _ response:URLResponse?,  _ error:NSError) -> Void
    func setImageWithUrlRequest(_ request:URLRequest, placeHolderImage:UIImage? = nil,
        success:SuccessHandler?,
        failure:FailureHandler?)
    {
        self.cancelImageRequestOperation()
        
        if let cachedImage = UIImageView.sharedImageCache().cachedImageForRequest(request) {
            if let s = success  {
                s(nil, nil, cachedImage)
            }
            else {
                self.image = cachedImage
            }
            
            return
        }
        
        //        if placeHolderImage != nil {
        self.image = placeHolderImage
        //        }
        
        self.af_requestImageOperation = (BlockOperation(block: { () -> Void in
            var response:URLResponse?
            var error:NSError?
            let data: Data?
            do {
                data = try NSURLConnection.sendSynchronousRequest(request, returning: &response)
            } catch let error1 as NSError {
                error = error1
                data = nil
            } catch {
                fatalError()
            }
            let image:UIImage? = (data != nil ? UIImage(data: data!) : nil)
            if(image != nil){
                UIImageView.sharedImageCache().cacheImage(image!, forRequest: request)
            }
            DispatchQueue.main.async(execute: { () -> Void in
                if request.url! == self.af_requestImageOperation.request?.url {
                    
                    if image != nil {
                        if success != nil {
                            success!(request, response, image!)
                        }
                        else {
                            self.image = image!
                        }
                    }
                    else {
                        if failure != nil {
                            failure!(request, response, error!)
                        }
                    }
                    
                    self.af_requestImageOperation = (nil, nil)
                }
            })
        }), request)
        
        UIImageView.af_sharedImageRequestOperationQueue().addOperation(self.af_requestImageOperation.operation!)
    }
    
    fileprivate func cancelImageRequestOperation() {
        self.af_requestImageOperation.operation?.cancel()
        self.af_requestImageOperation = (nil, nil)
    }
}

func AFImageCacheKeyFromURLRequest(_ request:URLRequest) -> String {
    return request.url!.absoluteString
}

open class AFImageCache: NSCache<AnyObject, AnyObject>, AFImageCacheProtocol {
    func cachedImageForRequest(_ request: URLRequest) -> UIImage? {
        switch request.cachePolicy {
        case NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData,
        NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData:
            return nil
        default:
            break
        }
        
        let url :String = AFImageCacheKeyFromURLRequest(request)
        return cachedImageForUrl(url)
    }
    
    open func cachedImageForUrl(_ url:String)->UIImage?{
        //        Swift.print("Looking for URL in cache \(url) -> ")
        if let memCached = self.object(forKey: url as AnyObject) as? UIImage{
            //            Swift.print("found in memory")
            return memCached
        }
        
        if let image:UIImage = getImageFromDiskForKey(url){
            //            Swift.print("found in disk")
            saveToMemory(image, key: url)
            return image
        }
        
        //        Swift.print("not found :(")
        
        return nil
        
    }
    
    open func cacheImage(_ image: UIImage, forRequest request: URLRequest) {
        let key :String = AFImageCacheKeyFromURLRequest(request)
        saveToDisk(image, url: key)
        saveToMemory(image, key: key)
    }
    
    func saveToMemory(_ image:UIImage, key:String){
        //        println("Saving URL image to memory cache: \(key)")
        self.setObject(image, forKey: key as AnyObject)
    }
    
    func saveToDisk (_ image:UIImage, url:String) {
        let path = (NSHomeDirectory() as NSString).appendingPathComponent("Library/Caches/\(url.hash).jpg")
        //        println("Saving URL image to disk cache: \(url) -> \(path)")
        
        try? image.jpegData(compressionQuality: 0.9)!.write(to: URL(fileURLWithPath: path), options: [.atomic])
    }
    
    func getImageFromDiskForKey (_ key:String) -> UIImage?{
        let path = (NSHomeDirectory() as NSString).appendingPathComponent("Library/Caches/\(key.hash).jpg")
        let fileMng = FileManager.default
        if(fileMng.fileExists(atPath: path)){
            return UIImage(contentsOfFile: path)
        }
        return nil
    }
}
