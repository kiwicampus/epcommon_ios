//
//  BaseUserData.swift
//  Pods
//
//  Created by Yeisson Oviedo on 4/25/16.
//
//

import Foundation

open class BaseUserData{
    public static func userId(_ id:String?){
        BaseUserData.defaults().set(id, forKey: "userId")
    }
    
    public static func userId() -> String?{
        guard let id = BaseUserData.defaults().object(forKey: "userId") as? String else {return nil}
        return (id == "" ? nil : id)
    }

    
    public class func number(_ number:String){
        set(number as AnyObject?, forKey: "number")
    }
    
    public class func number() -> String{
        let number = string("number")
        return number
    }
    
    public class func name(_ name:String){
        set(name as AnyObject?, forKey: "name")
    }
    
    public class func name() -> String{
        let name = string("name")
        return name
    }
    
    public class func email(_ email:String){
        set(email as AnyObject?, forKey: "email")
    }
    
    public class func email() -> String{
        let email = string("email")
        return email
    }
    
    public class func token(_ token:String){
        set(token as AnyObject?, forKey: "token")
    }
    
    public class func token() -> String{
        let token = string("token")
        return token
    }
    
    public class func remove (_ key:String){
        defaults().removeObject(forKey: key)
    }
    
    public class func contains (_ key:String) -> Bool {
        return defaults().object(forKey: key) != nil
    }
    
    public class func string(_ key:String) -> String{
        return defaults().string(forKey: key) ?? ""
    }
    
    public class func set(_ value:AnyObject?, forKey key:String){
        defaults().set(value, forKey: key)
    }
    
    public class func defaults() -> UserDefaults{
        return UserDefaults.standard
    }
    
}
