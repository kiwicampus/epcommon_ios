//
//  ViewController.swift
//  EPCommon
//
//  Created by Yeisson Oviedo on 04/15/2016.
//  Copyright (c) 2016 Yeisson Oviedo. All rights reserved.
//

import UIKit
import EPCommon

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Util.rootViewController = self
        _ = Util.Alert.showBusy()
        imageView.setImageWithUrlString("https://5.imimg.com/data5/SN/GS/MY-9217649/fresh-kiwi-fruit-500x500.jpg")
        
        Util.Run.afterDelay(1){
            Util.Alert.hideBusy()
            Util.Alert.showToast("Hello");
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func animateErrorAction(_ sender: AnyObject) {
        Util.Animation.defaultErrorAnimation = .spring
        Util.Animation.onError(textField)
        Util.Run.afterDelay(1){
            Util.Animation.onError(self.label)
        }
        
    }
    
}

